package me.benjoseph.memorygame;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class GameEngine {

    private static Set<Integer> getImageSet(int n) {
        Set<Integer> imageSet = new HashSet<Integer>();
        imageSet.add(R.drawable.ic_facebook_logo);
        imageSet.add(R.drawable.ic_instagram_logo);
        imageSet.add(R.drawable.ic_twitter_logo_button);
        return imageSet;
    }


    public static int[] getBoardArrangement(int m, int n) {
        int board[] = new int[m * n];
        for (Integer imageId : getImageSet((m * n) / 2)) {
            int added = 0;
            while (added < 2) {
                int index = new Random().nextInt(m * n);
                if (board[index] == 0) {
                    board[index] = imageId;
                    added++;
                }

            }
        }
        return board;
    }
}
