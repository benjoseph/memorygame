package me.benjoseph.memorygame;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private GridView mGridView;
    private Button mStartGameBtn;
    int mItemShown = -1;
    View mShownView;
    int[] mBoardArrangement;
    TextView mTimeTv;
    public int seconds = 60;
    public int minutes = 2;
    int time = 59;
    int row = 3;
    int col = 2;
    int answers;
    CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBoardArrangement = GameEngine.getBoardArrangement(row, col);
        mGridView = findViewById(R.id.grid_view);
        mStartGameBtn = findViewById(R.id.startGameBtn);
        mTimeTv = findViewById(R.id.timer);
        mStartGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimer();
                mGridView.setAdapter(new Adapter(getBaseContext()));
                mGridView.setOnItemClickListener(getAdapterItemClickListener());
            }

        });
    }

    private void setTimer() {

        timer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTimeTv.setText("0:" + String.valueOf(time));
                time--;
            }

            public void onFinish() {
                mTimeTv.setText("try again");
            }

        };
        timer.start();
    }

    private AdapterView.OnItemClickListener getAdapterItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    final View v, int position, long id) {
                ((ImageView) (v)).setImageResource(mBoardArrangement[position]);
                if (position == mItemShown) {
                    return;
                }
                if (mItemShown >= 0) {
                    if (mBoardArrangement[position] == mBoardArrangement[mItemShown]) {
                        // TODO: 09/03/19 both are checked
                        answers++;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((ImageView) (v)).setImageResource(R.drawable.ic_success);
                                ((ImageView) (mShownView)).setImageResource(R.drawable.ic_success);
                                mShownView = null;
                            }
                        }, 250);
                    } else {
                        // TODO: 09/03/19 both should be question mark
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((ImageView) (v)).setImageResource(R.drawable.ic_question_mark);
                                ((ImageView) (mShownView)).setImageResource(R.drawable.ic_question_mark);
                                mShownView = null;
                            }
                        }, 400);
                    }
                    mItemShown = -1;
                } else {
                    mItemShown = position;
                    mShownView = v;
                }
                if (answers >= (row * col) / 2) {
                    timer.cancel();
                    mTimeTv.setText("You won");
                }
            }
        };

    }

}
